﻿namespace _2_MappingProjection.Models
{
    public class WifiReport
    {
        public string Name { get; set; }
        public GeoCoordinate Coordinate { get; set; }
    }

    public class GeoCoordinate
    {
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}


